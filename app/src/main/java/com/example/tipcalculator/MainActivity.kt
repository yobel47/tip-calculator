package com.example.tipcalculator

import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import com.example.tipcalculator.databinding.ActivityMainBinding
import com.google.android.material.slider.Slider
import kotlin.math.ceil

class MainActivity : AppCompatActivity() {

    private lateinit var binding : ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        onTextChangeListener()
        hideOnEnter()
        changeTvTip()
        changeTvSplit()
        onSwitchChecked()
        calculate()
    }

    private fun onTextChangeListener(){
        binding.edCost.editText?.doOnTextChanged { _, _, _, _ ->
            calculate()
        }
    }

    private fun hideOnEnter(){
        binding.edCost.editText?.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                binding.edCost.editText?.clearFocus()
            }
            false
        }
    }

    private fun hideKeyboard(view: View?) {
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view?.windowToken, 0)
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val v: View? = currentFocus
            if (v is EditText) {
                val outRect = Rect()
                v.getGlobalVisibleRect(outRect)
                if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                    Log.d("focus", "touchevent")
                    v.clearFocus()
                    hideKeyboard(v)
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }

    private fun changeTvTip(){
        binding.sldTip.addOnSliderTouchListener(object : Slider.OnSliderTouchListener {
            override fun onStartTrackingTouch(slider: Slider) {
            }

            override fun onStopTrackingTouch(slider: Slider) {
                val value = slider.value.toInt()
                binding.tvTipVal.text = getString(R.string.tip_val, value)
                calculate()
            }
        })
    }

    private fun changeTvSplit(){
        binding.sldSplit.addOnSliderTouchListener(object : Slider.OnSliderTouchListener {
            override fun onStartTrackingTouch(slider: Slider) {
            }

            override fun onStopTrackingTouch(slider: Slider) {
                val value = slider.value.toInt()
                binding.tvSplitVal.text = resources.getQuantityString(R.plurals.split_val, value, value)
                calculate()
            }
        })
    }

    private fun onSwitchChecked(){
        binding.swcRound.setOnCheckedChangeListener { _, _ ->
            calculate()
        }
    }

    private fun calculate(){
        val cost = binding.edCost.editText?.text
        val tip = binding.sldTip.value
        val split = binding.sldSplit.value
        val round = binding.swcRound.isChecked

        val totalPerson: Float = if(cost!!.isEmpty()){
            0f
        }else{
            if(round){
                val temp = (Integer.parseInt(cost.toString()) * (tip/100))/split
                val count = ceil(temp/500)
                count * 500
            }else{
                ((Integer.parseInt(cost.toString()) * (tip/100))/split)
            }
        }
        binding.tvEachPersonVal.text = getString(R.string.total_val, totalPerson.toInt())
        val total = totalPerson * split
        binding.tvTotalVal.text = getString(R.string.total_val, total.toInt())

    }



}

